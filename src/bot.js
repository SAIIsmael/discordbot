require('dotenv').config();
const fs = require('fs');
const { Client } = require('discord.js');
const bot = new Client();
const PREFIX = "!";

function pad(n, length) {
    var s = n + "", needed = length - s.length;
    if (needed > 0) s = (Math.pow(10, needed) + "").slice(1) + s;
    return s;
}


function formatting(exams, id) {
    res = "+------------------------------------------------------+\n" +
        "|                CCEXAMS                               |\n" +
        "+-----+-------------------------------+---------+------+\n" +
        "| ID  | DATE                          | CODE    | TYPE |\n" +
        "+-----+-------------------------------+---------+------+\n";

    for (let index = 0; index < exams.length; index++) {
        const element = exams[index];
        let id = element.ID;
        let date = element.DATE;
        let code = element.CODE;
        let type = element.TYPE;

        res += "| " + id + " | " + date + "                      | " + code + " | " + type + "   |\n" +
            "+-----+-------------------------------+---------+------+\n";
    }
    return res;

}

bot.on('ready', () => {
    console.log(`${bot.user.username} ehas logged in`);
})

bot.on('message', (message) => {
    if (message.author.bot) return;
    if (message.content.startsWith(PREFIX)) {
        const [CMD_NAME, ...args] = message.content
            .trim()
            .substring(PREFIX.length)
            .split(/\s+/);


        if (CMD_NAME === 'help') {
            message.channel.send("available cmds : \n " +
                "-!help -> list of commands \n" +
                "- !see -> list of CCs and exams \n" +
                "- !add [date ueCode type] -> add an exam to the list \n" +
                "- !getInfo [ID] -> get extra info on exam by ID\n" +
                "- !setInfo [ID INFO] -> set extra info on exam by ID\n" +
                "- !delete [ID] -> delete exam by ID\n");
        } else if (CMD_NAME === 'see') {
            fs.readFile("./cache/CCEXAM", function read(err, data) {
                let result = formatting(JSON.parse(data.toString("utf-8")).exams);
                message.channel.send("```\n" + result + "\n```");
            })

        } else if (CMD_NAME == 'add') {
            if (args.length < 3) {
                message.channel.send("ERROR! \n Usage : !add [date ueCode type]: add an exam to the list ");
            } else {
                const dateRegex = /[0-9][0-9]\/[0-9][0-9]\/[0-9][0-9]/gm;
                let date = args[0].match(dateRegex);
                if (date === '' || args[1].length != 7 || args[2].length != 2) {
                    message.channel.send("ERROR! \n " +
                        "Date format : JJ/MM/AAAA \n" +
                        "UE SIZE MUST BE 7 (expl : HMIN306 \n" +
                        "TYPE SIZE MUST BE 2 (CC for CC/EX for exam/TP for TP");
                } else {
                    fs.readFile("./cache/CCEXAM", function read(err, data) {
                        let result = JSON.parse(data.toString("utf-8"));
                        let examToAdd = {
                            "ID": pad(result.GLOBAL_ID, 3),
                            "DATE": args[0],
                            "CODE": args[1],
                            "TYPE": args[2],
                            "INFO": ""
                        }
                        result.exams.push(examToAdd);
                        result.GLOBAL_ID++;
                        console.log(result);
                        fs.writeFile('./cache/CCEXAM', JSON.stringify(result), (err) => {
                            message.channel.send("successfully added to the CCEXAM");
                        })

                    })
                }
            }
        } else if (CMD_NAME == "getInfo") {
            if (args.length != 1) {
                message.channel.send("ERROR! \n Usage : !getInfo [ID] ");
            } else {
                fs.readFile("./cache/CCEXAM", function read(err, data) {
                    let result = JSON.parse(data.toString("utf-8"));
                    for (let index = 0; index < result.exams.length; index++) {
                        const element = result.exams[index];
                        if (element.ID == pad(args[0], 3)) {
                            message.channel.send("[" + element.CODE + ":" + element.DATE + "] " + element.INFO);
                        }
                    }
                })
            }
        } else if (CMD_NAME == "setInfo") {
            if (args.length < 2) {
                message.channel.send("ERROR! \n Usage : !setInfo [ID INFO] ");
            } else {
                let info = "";
                for (let index = 1; index < args.length; index++) {
                    const element = args[index];
                    console.log(element);
                    info += element + " ";
                }
                console.log(info);
                fs.readFile("./cache/CCEXAM", function read(err, data) {
                    let result = JSON.parse(data.toString("utf-8"));
                    for (let index = 0; index < result.exams.length; index++) {
                        const element = result.exams[index];
                        if (element.ID == pad(args[0], 3)) {
                            element.INFO = info;
                            fs.writeFile('./cache/CCEXAM', JSON.stringify(result), (err) => {
                                message.channel.send("info added successfully");
                            })
                        }
                    }
                })
            }

        } else if (CMD_NAME == "delete") {
            if (args.length != 1) {
                message.channel.send("ERROR! \n Usage : !delete [ID] ");
            } else {
                fs.readFile("./cache/CCEXAM", function read(err, data) {
                    let result = JSON.parse(data.toString("utf-8"));
                    let newExams = [];
                    for (let index = 0; index < result.exams.length; index++) {
                        const element = result.exams[index];
                        if (element.ID != pad(args[0], 3)) {
                            newExams.push(element);
                        }
                    }
                    result.exams = newExams;
                    fs.writeFile('./cache/CCEXAM', JSON.stringify(result), (err) => {
                        message.channel.send("delete has been ex");
                    })
                })
            }
        } else if (CMD_NAME == "udemyAdd") {
            if (args.length != 2) {
                message.channel.send("ERROR! \n Usage : !udemyAdd [ID PASS] ");
            } else {
                fs.readFile("./cache/UDEMY", function read(err, data) {
                    let result = JSON.parse(data.toString("utf-8"));
                    result.push({ "username": args[0], "pass": args[1], "status": "free" });

                    fs.writeFile("./cache/UDEMY", JSON.stringify(result), (err) => {
                        message.channel.send("udemy account added");
                    })
                })
            }
        } else if (CMD_NAME == "udemySee") {
            fs.readFile("./cache/UDEMY", function read(err, data) {
                let result = JSON.parse(data.toString("utf-8"));
                console.log(result);
                var messBuilder = "";
                for (let index = 0; index < result.length; index++) {
                    const element = result[index];
                    let uname = element.username;
                    let pass = element.pass;
                    messBuilder += "uname : " + uname + " | pass : " + pass + "| status : " + element.status + "\n";
                }
                console.log(messBuilder);
                message.channel.send(messBuilder);
            })
        } else if (CMD_NAME == "udemySetStatus") {
            if (args.length != 3) {
                message.channel.send("ERROR! \n Usage : !udemySetStatus [uname USED/FREE yourName]")
            } else {
                fs.readFile("./cache/UDEMY", function read(err, data) {
                    let result = JSON.parse(data.toString("utf-8"));
                    for (let index = 0; index < result.length; index++) {
                        const element = result[index];
                        if (element.username == args[0]) {
                            element.status = args[1] + "(" + args[2] + ")";
                            break;
                        }
                    }
                    fs.writeFile("./cache/UDEMY", JSON.stringify(result), (err) => {
                        message.channel.send("udemy status edited");
                    })
                })
            }
        } else if (CMD_NAME == "udemyGetStatus") {
            if (args.length != 1) {
                message.channel.send("ERROR! \n Usage : !udemyGetStatus [uname]")
            } else {
                fs.readFile("./cache/UDEMY", function read(err, data) {
                    let result = JSON.parse(data.toString("utf-8"));
                    for (let index = 0; index < result.length; index++) {
                        const element = result[index];
                        console.log(element.username + " " + args[0])
                        if (element.username.localeCompare(args[0]) == 0) {
                            message.channel.send(element.username + " - " + element.status);
                            break;
                        }
                    }
                })
            }
        } else if (CMD_NAME == "udemyHelp") {
            message.channel.send("PLEASE don't forget to set status on used/free while you're using/done using the account \n" +
                "NOTE that you have to write down where you are in the course because many of us are using the account and therefore the automatic progression will be obsolete\n" +
                "USAGE : \n " +
                "!udemySee - see the udemy accounts and the status \n" +
                " !udemyAdd [ID PASS] (expl : !udemyAdd test@test.fr dony) - add an account \n" +
                " !udemyGetStatus [uname] (expl : !udemyGetStatus test@test.fr) - see status of an account \n" +
                " !udemySetStatus [uname USED/FREE yourName] (expl : !udemySetStatus ismaelsai@yahoo.com USED isma) - set status of an account \n");
        }
    }
})
bot.login(process.env.DISCORDJS_BOT_TOKEN);
