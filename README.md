# DiscordBot

Just trying to have fun with discord.js thanks to [Traversy Media's youtube channel](https://www.youtube.com/watch?v=BmKXBVdEV0g) .

# Commands available 


|                 COMMANDS      |         DESC            |
| ------------------------------| ------------------------|
| !help                         | list of commands        |
| !see                          | see list of exams       |
| !add [date code type]         | add an exam to the list |
| !getInfo [ID]                 | edit info of exam by ID |
| !setInfo [ID INFO]            | set info of exam by ID  |
| !delete [ID]                  | delete exam by ID       |

-----
![Example](https://i.imgur.com/Vf2UYtv.png)


# How it works 

thanks to a cache file, when the see command is called the file is read and the result is sent to the current channel. When calling add, if the command has the right format, the text is formatted as a table and is appended to the cache file.

# How tu use it

You should create a .env file in the main repository and add your tokken  in var *DISCORDJS_BOT_TOKEN*

Have fun.